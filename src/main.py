# main.py
#
# Copyright 2022 px
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')

from gi.repository import Gtk, Gio, Adw
from .window import MatineeWindow

class MatineeApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self):
        super().__init__(application_id='org.yphil.matinee',
                         flags=Gio.ApplicationFlags.HANDLES_OPEN)
        self.create_action('quit', self.on_quit, ['<primary>q'])
        self.create_action('about', self.on_about_action)
        self.create_action('preferences', self.on_preferences_action)


    def do_activate(self):
        """
        We raise the application's main window, creating it if
        necessary.
        """

        win = self.props.active_window
        if not win:
            win = MatineeWindow(application=self)
            self.video = win.video

        try:
            self.file = self.files[0]
        except AttributeError:
            self.file = "/home/px/tmp/SAPTR12-360p.mp4"
        self.load_video_file(self.file)

        win.present()

    def on_eos(self, widget, args):
        print("args:", args)

    def tezt(self):
        print("Test: ", self.playlistLabel.get_label())

    def load_video_file(self, file):

        file_ok = False

        if isinstance(file, Gio.File):
            file_ok = True
            stream = Gtk.MediaFile.new_for_file(file)
        else:
            if os.path.exists(file):
                print("Ok: ", file)
                stream = Gtk.MediaFile.new_for_filename(file)
                file_ok = True

        if file_ok:
            self.video.set_media_stream(stream)
            stream.play()
            stream.connect("notify::ended", self.on_eos)
            #r_dur = stream.get_duration()
        else:
            print("Not ok:", file)


        return True

    def do_open(self, files, hint, other):
        self.files = files
        self.do_activate()

    def on_quit(self, widget, _):
        self.quit()

    def on_about_action(self, widget, _):
        """Callback for the app.about action."""
        about = Adw.AboutWindow(transient_for=self.props.active_window,
                                application_name='matinee',
                                application_icon='org.yphil.matinee',
                                developer_name='px',
                                version='0.1.0',
                                developers=['px'],
                                copyright='© 2022 px')
        about.present()

    def on_preferences_action(self, widget, _):
        """Callback for the app.preferences action."""
        print('app.preferences action activated')

    def create_action(self, name, callback, shortcuts=None):
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
        """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    """The application's entry point."""
    app = MatineeApplication()
    return app.run(sys.argv)

